export default {
  data() {
    return {
      monthNames: [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
      ]
    };
  },
  methods: {
    sumAllValuesInArrayOfObjects(data) {
      var result = [
        data.reduce((acc, n) => {
          for (var prop in n) {
            if (acc.hasOwnProperty(prop)) acc[prop] += parseFloat(n[prop] || 0);
            else acc[prop] = parseFloat(n[prop] || 0);
          }
          return acc;
        }, {})
      ];
      return result;
    }
  },
  mounted() {}
};
