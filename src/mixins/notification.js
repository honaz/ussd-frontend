
import { Notify } from 'quasar'

export const notification = {
  data() {
    return {
      timeout:  4000,
    };
  },

  methods: {
    success(message = "action completed successfully") {
      Notify.create({
        title: "Success",
        icon: "sentiment_satisfied_alt",
        message: message,
        color: "green",
        textColor: "white",
        timeout: this.timeout,
        actions: [
          {
            icon: "delete",
            color: "white",
            handler: () => { }
          }
        ]
      });
    },
    error(reason) {
      Notify.create({
        title: "Failed",
        icon: "sentiment_very_dissatisfied",
        message: reason,
        color: "red",
        textColor: "white",
        actions: [
          {
            icon: "delete",
            color: "white",
            handler: () => { }
          }
        ]
      });
    },
    info(message , title = "Description") {
      this.$q.dialog({
        title: title,
        html: true,
        message: message
      });
    }
  }
};
