
export const format = {
  data() {
    return {
    };
  },

  methods: {
    arrayToString(objArray) {
      return objArray.join(",")
    },
    sum(obj) {
      return Object.keys(obj).reduce((sum, key) => sum + parseFloat(obj[key] || 0), 0);
    },
    money(value) {
      let val = (value / 1).toFixed(2).replace(",", ".");
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
  }
};
