import { SessionStorage, colors } from "quasar";
const serial = process.env.SERIAL;

const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      // {
      //   path: "/",
      //   name: "home",
      //   component: () => import("../packages/dashboard/index.vue")
      // },
      {
        path: "/",
        name: "login",
        component: () => import("../pages/login.vue")
      },
      {
        path: "/register",
        name: "register",
        component: () => import("../pages/register.vue")
      },
      {
        path: "/reset",
        name: "reset",
        component: () => import("../pages/reset.vue")
      }
    ]
  },

  //Admin
  {
    path: "/admin",
    name: "admin",
    component: () => import("../packages/admin/layouts/AdminLayout.vue"),
    children: [
      //Home
      {
        path: "/admin",
        component: () => import("../packages/admin/pages/home/index.vue")
      },
      //Accounts
      {
        path: "/admin/accounts",
        component: () => import("../packages/admin/pages/accounts/index.vue")
      },
      {
        path: "/admin/accounts/manage/:id",
        props: true,
        component: () => import("../packages/admin/pages/accounts/manage.vue")
      },
      //organizations
      {
        path: "/admin/organizations",
        component: () =>
          import("../packages/admin/pages/organizations/index.vue")
      },
      {
        path: "/admin/organizations/manage/:id",
        props: true,
        component: () =>
          import("../packages/admin/pages/organizations/manage.vue")
      },
      //Settings
      {
        path: "/admin/settings",
        component: () => import("../packages/admin/pages/settings/index.vue")
      }
    ],
    beforeEnter: (to, from, next) => {
      let role = SessionStorage.getItem(serial + "-ROLE");
      if (SessionStorage.has(serial + "-JWT") == true && role.name == "admin") {
        next();
      } else {
        next("/login");
      }
    }
  },

  //Manager
  {
    path: "/manager",
    name: "manager",
    component: () => import("../packages/manager/layouts/ManagerLayout.vue"),
    children: [
      //Home
      {
        path: "/manager",
        component: () => import("../packages/manager/pages/home/index.vue")
      },
      //ussd
      {
        path: "/manager/ussd",
        component: () => import("../packages/manager/pages/ussd/index.vue")
      },
      //Accounts
      {
        path: "/manager/accounts",
        component: () => import("../packages/manager/pages/accounts/index.vue")
      },
      {
        path: "/manager/accounts/manage/:id",
        props: true,
        component: () => import("../packages/manager/pages/accounts/manage.vue")
      },
      //organization
      {
        path: "/manager/organization",
        component: () =>
          import("../packages/manager/pages/organization/index.vue")
      },
      //Data
      {
        path: "/manager/stores",
        component: () => import("../packages/manager/pages/store/index.vue")
      },
      {
        path: "/manager/stores/manage/:id",
        props: true,
        component: () => import("../packages/manager/pages/store/manager.vue")
      },
      {
        path: "/manager/stores/data/:id",
        props: true,
        component: () => import("../packages/manager/pages/store/data.vue")
      },
      //Settings
      {
        path: "/manager/settings",
        component: () => import("../packages/manager/pages/settings/index.vue")
      }
    ],
    beforeEnter: (to, from, next) => {
      let role = SessionStorage.getItem(serial + "-ROLE");
      if (
        SessionStorage.has(serial + "-JWT") == true &&
        role.name == "manager"
      ) {
        next();
      } else {
        next("/login");
      }
    }
  },

  //Coordinator
  {
    path: "/coordinator",
    name: "coordinator",
    component: () =>
      import("../packages/coordinator/layouts/CoordinatorLayout.vue"),
    children: [
      //Home
      {
        path: "/coordinator",
        component: () => import("../packages/coordinator/pages/home/index.vue")
      },
      //Accounts
      {
        path: "/coordinator/accounts",
        component: () =>
          import("../packages/coordinator/pages/accounts/index.vue")
      },
      {
        path: "/coordinator/accounts/manage/:id",
        props: true,
        component: () =>
          import("../packages/coordinator/pages/accounts/manage.vue")
      },
      //Data
      //Data
      {
        path: "/coordinator/stores",
        component: () => import("../packages/coordinator/pages/store/index.vue")
      },
      {
        path: "/coordinator/stores/manage/:id",
        props: true,
        component: () =>
          import("../packages/coordinator/pages/store/manager.vue")
      },
      {
        path: "/coordinator/stores/data/:id",
        props: true,
        component: () => import("../packages/coordinator/pages/store/data.vue")
      },
      //Settings
      {
        path: "/coordinator/settings",
        component: () =>
          import("../packages/coordinator/pages/settings/index.vue")
      }
    ],
    beforeEnter: (to, from, next) => {
      let role = SessionStorage.getItem(serial + "-ROLE");
      if (
        SessionStorage.has(serial + "-JWT") == true &&
        role.name == "coordinator"
      ) {
        next();
      } else {
        next("/login");
      }
    }
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
