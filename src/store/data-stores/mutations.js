export const set = (state, val) => {
  state.stores = val;
};

export const update = (state, val) => {
  state.stores.splice(state.stores.map(o => o.id).indexOf(val.id), 1, val);
};

export const remove = (state, val) => {
  state.stores = state.stores.filter(x => {
    return x.id != val;
  });
};

export const setOne = (state, val) => {
  state.stores.push(val);
};

export const setStores = (state, val) => {
  state.stores = val;
};

export const setLocationId = (state, val) => {
  state.locationId = val;
};
