import axios from "axios";
const resource = process.env.ENGINE_API;

export const get = async(context, id) => {
    let organization = context.rootGetters["session/getOrganization"];
    if (id == null) {
        return await axios
            .get(
                resource +
                "/stores" +
                '?filter={"include":"fields","where":{"organizationId":"' +
                organization.id.toString() +
                '"}}&access_token=' +
                context.rootGetters["session/getToken"]
            )
            .then(response => {
                context.commit("set", response.data);
                return response.data;
            });
    } else if (id != null) {
        let result = context.getters.getById(id);
        if (typeof result == "undefined") {
            return await axios
                .get(
                    resource +
                    "/stores" +
                    "/" +
                    id +
                    '?filter={"include":"fields","where":{"organizationId":"' +
                    organization.id.toString() +
                    '"}}&access_token=' +
                    context.rootGetters["session/getToken"]
                )
                .then(response => {
                    return response.data;
                });
        } else {
            return result;
        }
    }
};

export const count = async(context, data) => {
    //let locationId = context.rootGetters["session/getUser"].admin2Id;
    let organization = context.rootGetters["session/getOrganization"];
    let response = await axios
        .get(
            resource +
            '/stores/count?filter={"where":{"organizationId":"' +
            organization.id.toString() +
            '"}}&access_token=' +
            context.rootGetters["session/getToken"]
        )
        .then(response => {
            return response.data;
        });

    return response;
};

export const create = async(context, data) => {
    let organization = context.rootGetters["session/getOrganization"];
    data.organizationId = organization.id;
    let fields = data.fields;
    delete data.fields;
    return await axios
        .post(
            resource +
            "/stores" +
            "?&access_token=" +
            context.rootGetters["session/getToken"],
            data
        )
        .then(response => {
            let store = response.data;
            // create fields
            let storeFields = [];
            for (let i = 0; i < fields.length; i++) {
                let field = fields[i];
                field.storeId = store.id;
                storeFields.push(field);
            }
            context.dispatch("createFields", storeFields);
            store.fields = storeFields;
            context.commit("setOne", store);
            return data;
        });
};

export const update = async(context, data) => {
    return await axios
        .patch(
            resource +
            "/stores" +
            "/" +
            data.id +
            "?&access_token=" +
            context.rootGetters["session/getToken"],
            data
        )
        .then(response => {
            if (context.state.stores.length != 0) {
                context.commit("update", data);
            }
            return response.data;
        });
};

export const remove = async(context, data) => {
    return await axios
        .delete(
            resource +
            "/stores" +
            "/" +
            data +
            "?&access_token=" +
            context.rootGetters["session/getToken"],
            data
        )
        .then(response => {
            context.commit("remove", data);
            return response.data;
        });
};

// fields

export const createFields = async(context, data) => {
    return await axios
        .post(
            resource +
            "/fields" +
            "?&access_token=" +
            context.rootGetters["session/getToken"],
            data
        )
        .then(response => {
            return data;
        });
};
//locations
export const setLocationId = async(context, data) => {
    context.commit("setLocationId", data);
};
// records by location
export const getRecords = async(context, id) => {
    return await axios
        .get(
            resource +
            "/records" +
            '?filter={"include":["values","account"],"where":{"storeId":"' +
            id.toString() +
            '"}}&&access_token=' +
            context.rootGetters["session/getToken"]
        )
        .then(response => {
            console.log(response);
            var records = response.data;
            records = records.filter(item => {
                //console.log();
                return (
                    item.admin2Id === context.rootGetters["session/getUser"].admin2Id
                )
            }).slice()
            .reverse();
            context.commit("setStores", records);
            //console.log(records);
            return records;
        });
};
// records by all
// records

export const getAllRecords = async(context, id) => {
    return await axios
        .get(
            resource +
            "/records" +
            '?filter={"include":["values","account"],"where":{"storeId":"' +
            id.toString() +
            '"}}&&access_token=' +
            context.rootGetters["session/getToken"]
        )
        .then(response => {
            var records = response.data;
            records = records.filter(item => {
                //console.log();
                return item.admin2Id !== "";
            }).slice()
            .reverse();
            context.commit("setStores", records);
            //console.log(records, "Honest");
            return records;
        });
};

export const getByDistrict = async(context, id) => {
    return await axios
        .get(
            resource +
            "/records" +
            '?filter={"include":["values","account"],"where":{"storeId":"' +
            id.toString() +
            '"}}&&access_token=' +
            context.rootGetters["session/getToken"]
        )
        .then(response => {
            return response.data;
        });
};

export const getStores = async(context, id) => {
    let organization = context.rootGetters["session/getOrganization"];
    if (id == null) {
        return await axios
            .get(
                resource +
                "/stores" +
                '?filter={"include":"fields","where":{"organizationId":"' +
                organization.id.toString() +
                '"}}&access_token=' +
                context.rootGetters["session/getToken"]
            )
            .then(response => {
                console.log(response.data);
                context.commit("set", response.data);
                return response.data;
            });
    } else if (id != null) {
        let result = context.getters.getById(id);
        if (typeof result == "undefined") {
            return await axios
                .get(
                    resource +
                    "/stores" +
                    "/" +
                    id +
                    '?filter={"include":"fields","where":{"organizationId":"' +
                    organization.id.toString() +
                    '"}}&access_token=' +
                    context.rootGetters["session/getToken"]
                )
                .then(response => {
                    return response.data;
                });
        } else {
            return result;
        }
    }
};

export const getAllStores = async(context, id) => {
    // let organization = context.rootGetters["session/getOrganization"];
    if (id == null) {
        return await axios
            .get(
                resource +
                "/stores" +
                '?filter={"include":"fields"}&access_token=' +
                context.rootGetters["session/getToken"]
            )
            .then(response => {
                console.log(response.data);
                context.commit("set", response.data);
                return response.data;
            });
    } else if (id != null) {
        let result = context.getters.getById(id);
        if (typeof result == "undefined") {
            return await axios
                .get(
                    resource +
                    "/stores" +
                    "/" +
                    id +
                    '?filter={"include":"fields","where":{"organizationId":"' +
                    organization.id.toString() +
                    '"}}&access_token=' +
                    context.rootGetters["session/getToken"]
                )
                .then(response => {
                    return response.data;
                });
        } else {
            return result;
        }
    }
};