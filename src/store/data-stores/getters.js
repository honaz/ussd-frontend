export const get = state => {
  return state.stores.slice().reverse();
};

export const getById = (state, getters) => id => {
  return getters.get.filter(item => item.id === id)[0];
};

export const getByDistrictId = (state, getters) => id => {
  return getters.get.filter(item => item.admin2Id === id);
};

export function getUser(state) {
  if (state.user != null) {
    // state
    return state.user;
  } else {
    // session
    let user = SessionStorage.getItem(process.env.SERIAL + "-USER");
    return user;
  }
}
