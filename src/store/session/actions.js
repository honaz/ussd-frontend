import axios from "axios";
import { LocalStorage, SessionStorage, Platform } from "quasar";
const resource = process.env.ENGINE_API + "/accounts";

export const authenticate = async (context, data) => {
  let response = await axios
    .get(
      resource +
        "/" +
        data.id +
        '/?filter={"include":["role","organization"]}&access_token=' +
        data.token
    )
    .then(response => {
      context.commit("setUser", response.data.account);
      context.commit("setRole", response.data.role);
      context.commit("setToken", response.data.id);
      context.commit("setOrganization", response.data.organization);
      return true;
    });
  return response;
};

export const login = async (context, data) => {
  let response = await axios
    .post(resource + '/login?filter={"include":["role","organization"]}', data)
    .then(response => {
      context.commit("setUser", response.data.account);
      context.commit("setRole", response.data.role);
      context.commit("setToken", response.data.id);
      context.commit("setOrganization", response.data.organization);
      return response.data;
    });
  return response;
};

export const check = async (context, data) => {
  if (Platform.is.electron === true || Platform.is.android === true) {
    if (
      LocalStorage.has(process.env.SERIAL + "-JWT") == true &&
      LocalStorage.has(process.env.SERIAL + "-ROLE") == true &&
      LocalStorage.has(process.env.SERIAL + "-USER") == true &&
      LocalStorage.has(process.env.SERIAL + "-ORGANIZATION") == true
    ) {
      context.commit(
        "setUser",
        LocalStorage.getItem(process.env.SERIAL + "-USER")
      );
      context.commit(
        "setRole",
        LocalStorage.getItem(process.env.SERIAL + "-ROLE")
      );
      context.commit(
        "setToken",
        LocalStorage.getItem(process.env.SERIAL + "-JWT")
      );
      context.commit(
        "setOrganization",
        LocalStorage.getItem(process.env.SERIAL + "-ORGANIZATION")
      );
    }
  }
  if (
    SessionStorage.has(process.env.SERIAL + "-JWT") == true &&
    SessionStorage.has(process.env.SERIAL + "-ROLE") == true &&
    SessionStorage.has(process.env.SERIAL + "-USER") == true &&
    SessionStorage.has(process.env.SERIAL + "-ORGANIZATION")
  ) {
    context.commit(
      "setUser",
      SessionStorage.getItem(process.env.SERIAL + "-USER")
    );
    context.commit(
      "setRole",
      SessionStorage.getItem(process.env.SERIAL + "-ROLE")
    );
    context.commit(
      "setToken",
      SessionStorage.getItem(process.env.SERIAL + "-JWT")
    );
    context.commit(
      "setOrganization",
      SessionStorage.getItem(process.env.SERIAL + "-ORGANIZATION")
    );
    return true;
  } else {
    return false;
  }
};

export const logout = async (context, data) => {
  let token = context.rootGetters["session/getToken"];
  SessionStorage.clear();
  LocalStorage.clear();

  await axios
    .post(resource + "/logout?&access_token=" + token)
    .then(response => {});
  return true;
};

export const update = async (context, data) => {
  return await axios
    .patch(
      resource +
        "/" +
        SessionStorage.getItem(process.env.SERIAL + "-USER").id +
        "?&access_token=" +
        SessionStorage.getItem(process.env.SERIAL + "-JWT"),
      data
    )
    .then(response => {
      return response.data;
    })
    .catch(error => {
      if (!error.response) {
        // network error
        error = "Error: Network Error";
      } else {
        return error.response.data.message;
      }
    });
};

export const changePassword = async (context, data) => {
  return await axios
    .post(
      resource +
        "/change-password" +
        "?&access_token=" +
        SessionStorage.getItem(process.env.SERIAL + "-JWT"),
      data,
      { headers: { "Content-Type": "application/x-www-form-urlencoded" } }
    )
    .then(response => {
      return response.data;
    })
    .catch(error => {
      if (!error.response) {
        // network error
        throw error;
      } else {
        return error.response.data.message;
      }
    });
};

export const getLogs = async (context, data) => {
  let response = await axios
    .get(
      resource +
        "/" +
        context.rootGetters["session/getUser"].id +
        "/logs?&access_token=" +
        context.rootGetters["session/getToken"]
    )
    .then(response => {
      return response.data;
    });
  return response;
};
