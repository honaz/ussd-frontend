export default function () {
  return {  
    user : null,
    role : null, 
    token : null,
    organization : null,
    configs:{
      color:'primary',
      drawer:'false',
      options:'minimised',
      background:'',
      tips:{
        firstLogin:true,
        map:false,
      }
    }
  }
}
