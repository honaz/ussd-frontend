import state from './state'
import * as getters from './getters'

export default {
  namespaced: true,
  getters,
  state
}
