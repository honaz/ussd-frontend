import axios from 'axios'
const resource = process.env.ENGINE_API;


export const get = async (context, id) => {

    if (id == null) {
        if (context.state.organizations.length == 0) {
            return await axios.get(resource + '/organizations' + '?filter[include]=organizationType&access_token=' + context.rootGetters['session/getToken']).then(response => {
                context.commit('set', response.data);
                return response.data;
            });
        }
        else {
            return context.state.organizations;
        }
    }
    else if (id != null) {
        let result = context.getters.getById(id);
        if (typeof result == "undefined") {
            return await axios.get(resource + '/organizations' + '/' + id + '?filter[include]=organizationType&access_token=' + context.rootGetters['session/getToken']).then(response => {
                return response.data;
            });
        } else {
            return result;
        }
    }
}



export const count = async (context, data) => {
    let response = await axios.get(resource + '/organizations/count?&access_token=' + context.rootGetters['session/getToken']).then(response => {
        return response.data;
    });
    return response
}


export const create = async (context, data) => {
    let organizationType = data.organizationType;
    delete data.organizationType;
    return await axios.post(resource + '/organizations' + '?&access_token=' + context.rootGetters['session/getToken'], data).then(response => {
        let organization = response.data
        organization.organizationType = organizationType;
        context.commit('setOne', organization);
        //create ussd menu 
        let menu = {
            name: organization.name_short + " USSD",
            is_main_menu: true,
            narrative: organization.name + " USSD main menu",
            organizationId: organization.id,
            options:[],
        }
        context.dispatch('createMainMenu', menu);

        return organization;
    });
}

export const update = async (context, data) => {
    return await axios.patch(resource + '/organizations' + '/' + data.id + '?&access_token=' + context.rootGetters['session/getToken'], data).then(response => {
        if (context.state.organizations.length != 0) {
            context.commit('update', data);
        }
        return response.data;
    });
}

export const remove = async (context, data) => {
    return await axios.delete(resource + '/organizations' + '/' + data + '?&access_token=' + context.rootGetters['session/getToken'], data).then(response => {
        context.commit('remove', data);
        return response.data;
    });
}

// type
export const getTypes = async (context, data) => {
    if (context.state.types.length == 0) {
        return await axios.get(resource + '/organization_types' + '?&access_token=' + context.rootGetters['session/getToken']).then(response => {
            context.commit('setTypes', response.data);
            return response.data;
        });
    } else {
        return context.state.types;
    }
}

// menu 
export const createMainMenu = async (context, data) => {
    return await axios.post(resource + '/organizations/'+data.organizationId+'/menus' + '?&access_token=' + context.rootGetters['session/getToken'], data).then(response => {
        return response.data;
    });
}
