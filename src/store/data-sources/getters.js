
    export const get = (state) => {
        return state.organizations.slice().reverse();
    }
    
    export const getById = (state, getters) => id => {
        return getters.get.filter(item => item.id === id)[0]
    }

    export const getByTypeId = (state, getters) => id => {
        return getters.get.filter(item => item.organizationTypeId === id)
    }

    export const getTypes = (state) => {
        return state.types
    }

   
  

    
    