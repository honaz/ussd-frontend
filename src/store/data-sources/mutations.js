
export const set = (state, val) => {
    state.organizations = val
}

export const update = (state, val) => {
    state.organizations.splice(state.organizations.map(o => o.id).indexOf(val.id), 1, val);
}

export const remove = (state, val) => {
    state.organizations = state.organizations.filter(x => {
        return x.id != val;
    })
}

export const setOne = (state, val) => {
    state.organizations.push(val)
}

// type

export const setTypes = (state, val) => {
    state.types = val
}


