export default function() {
  return {
    accounts: [],
    roles: [],
    locationId: null,
    organizationId: null
  };
}
