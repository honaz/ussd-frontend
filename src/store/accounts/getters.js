export const get = state => {
    // console.log(state.accounts);
    return state.accounts.slice().reverse();
};

export const getById = (state, getters) => id => {
    return getters.get.filter(item => item.id === id)[0];
};

//get users from specific district
export const getAccountsByLocation = (state, getters) => {
    return getters.get.filter(
        item =>
        item.admin2Id === state.locationId &&
        item.roleId === "4" &&
        item.organizationId === state.organizationId
    );
};

//get all account for specific district
export const getAllAccountsByLocation = (state, getters) => {
    return getters.get.filter(
        item =>
        //item.admin2Id === state.locationId &&
        item.organizationId === state.organizationId
    );
};

//get all users by roleid
export const getByRoleId = (state, getters) => id => {
    return getters.get.filter(item => item.roleId === id);
};

//get all users by roleid
export const getByRoleIdLocation = (state, getters) => id => {
    return getters.get.filter(
        item =>
        item.roleId === id &&
        item.organizationId === state.organizationId &&
        item.roleId !== "1"
    );
};

export const getRoles = state => {
    return state.roles;
};