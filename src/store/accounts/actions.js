import axios from "axios";
const resource = process.env.ENGINE_API;

export const get = async(context, id) => {
    if (id == null) {
        if (context.state.accounts.length == 0) {
            return await axios
                .get(
                    resource +
                    "/accounts" +
                    "?filter[include]=role&filter[include]=organization&filter[include]=admin2&filter[include]=logs&access_token=" +
                    context.rootGetters["session/getToken"]
                )
                .then(response => {
                    var accounts = response.data.filter(item => {
                        return item.id !== context.rootGetters["session/getUser"].id;
                    });
                    accounts = accounts.filter(item => {
                        return item.username !== "administrator";
                    });
                    context.commit("set", accounts);
                    return accounts;
                });
        } else {
            return context.state.accounts;
        }
    } else if (id != null) {
        let result = context.getters.getById(id);
        if (typeof result == "undefined") {
            return await axios
                .get(
                    resource +
                    "/accounts" +
                    "/" +
                    id +
                    "?filter[include]=role&filter[include]=organization&filter[include]=admin2&access_token=" +
                    context.rootGetters["session/getToken"]
                )
                .then(response => {
                    return response.data;
                });
        } else {
            return result;
        }
    }
};
export const getByUser = async(context, id) => {
    if (context.state.projects.length == 0) {
        return await axios
            .get(
                resource +
                "/accounts" +
                '?filter={"where":{"createdby.email":"' +
                id +
                '"}}&access_token=' +
                context.rootGetters["session/getToken"]
            )
            .then(response => {
                context.commit("set", response.data);
                return response.data;
            });
    } else {
        return context.state.projects;
    }
};

export const getByLocation = async context => {
    var locationId = context.rootGetters["session/getUser"].admin2Id;
    context.commit("setLocationId", locationId);
    // console.log(locationId);
    var organizationId = context.rootGetters["session/getOrganization"].id;
    context.commit("setOrganizationId", organizationId);
    //store locationId

    return await axios
        .get(
            resource +
            "/accounts" +
            "?filter[include]=role&filter[include]=organization&filter[include]=logs&filter[include]=admin2&access_token=" +
            context.rootGetters["session/getToken"]
        )
        .then(response => {
            var accounts = response.data.filter(item => {
                return item.id !== context.rootGetters["session/getUser"].id;
            });
            accounts = accounts.filter(item => {
                return (
                    item.username !== "administrator" &&
                    item.id !== context.rootGetters["session/getUser"].id
                );
            });
            context.commit("set", accounts);
            //console.log(accounts.filter(a => a.admin2Id === context.state.locationId))
            return accounts.filter(
                a =>
                // a.admin2Id === locationId &&
                a.organizationId === organizationId &&
                a.id !== context.rootGetters["session/getUser"].id
            );
        });
};

export const getUserByLocation = async context => {
    let locationId = context.rootGetters["session/getUser"].admin2Id;
    //store locationId
    context.commit("setLocationId", locationId);
    // console.log(locationId);
    var organizationId = context.rootGetters["session/getOrganization"].id;
    context.commit("setOrganizationId", organizationId);

    if (context.state.accounts.length == 0) {
        return await axios
            .get(
                resource +
                "/accounts" +
                "?filter[include]=role&filter[include]=organization&filter[include]=admin2&filter[include]=logs&access_token=" +
                context.rootGetters["session/getToken"]
            )
            .then(response => {
                var accounts = response.data.filter(item => {
                    return item.id !== context.rootGetters["session/getUser"].id;
                });
                accounts = accounts.filter(item => {
                    return item.username !== "administrator";
                });
                context.commit("set", accounts);
                //store locationId

                //console.log(accounts.filter(a => a.admin2Id === context.state.locationId))
                return accounts
                    .filter(
                        a =>
                        a.admin2Id === locationId &&
                        a.roleId === "4" &&
                        a.organizationId === organizationId
                    )
                    .slice()
                    .reverse();
            });
    } else {
        //console.log(context.state.accounts.filter(a => a.admin2Id === locationId));
        return context.state.accounts
            .filter(
                a =>
                a.admin2Id === locationId &&
                a.roleId === "4" &&
                a.organizationId === organizationId
            )
            .slice()
            .reverse();
    }
};

export const getByOrganizationId = async(context, id) => {
    let organization = context.rootGetters["session/getOrganization"];
    if (id == null) {
        if (context.state.accounts.length == 0) {
            return await axios
                .get(
                    resource +
                    "/accounts" +
                    '?filter={"include":["role","organization","logs"],"where":{"organizationId":{ "like" : "' +
                    organization.id.toString() +
                    '"}}}&access_token=' +
                    context.rootGetters["session/getToken"]
                )
                .then(response => {
                    var accounts = response.data.filter(item => {
                        return item.id !== context.rootGetters["session/getUser"].id;
                    });
                    accounts = accounts.filter(item => {
                        return item.username !== "administrator";
                    });
                    context.commit("set", accounts);
                    return accounts;
                });
        } else {
            return context.state.accounts;
        }
    } else if (id != null) {
        let result = context.getters.getById(id);
        if (typeof result == "undefined") {
            return await axios
                .get(
                    resource +
                    "/accounts" +
                    "/" +
                    id +
                    '?filter={"include":["role","organization"],"where":{"organizationId":"' +
                    organization.id.toString() +
                    '"}}&access_token=' +
                    context.rootGetters["session/getToken"]
                )
                .then(response => {
                    return response.data;
                });
        } else {
            return result;
        }
    }
};

export const count = async(context, data) => {
    let response = await axios
        .get(
            resource +
            "/accounts/count?&access_token=" +
            context.rootGetters["session/getToken"]
        )
        .then(response => {
            return response.data;
        });
    return response;
};

export const counts = async(context, data) => {
    let response = await axios
        .get(
            resource +
            "/accounts/count?&access_token=" +
            context.rootGetters["session/getToken"]
        )
        .then(response => {
            return response.data.filter(a => a.admin2Id === locationId);
        });
    console.log(response);
    return response;
};

// export const getAll = async (context, data) => {
//     if (context.state.accounts.length == 0) {
//         return await axios.get(resource + '/accounts' + '?&access_token=' + context.rootGetters['session/getToken']).then(response => {
//             context.commit('set', response.data);
//             return response.data;
//         });
//     }
//     else {
//         return context.state.accounts;
//     }

// }

export const create = async(context, data) => {
    return await axios
        .post(
            resource +
            "/accounts" +
            "?&access_token=" +
            context.rootGetters["session/getToken"],
            data
        )
        .then(response => {
            let user = response.data;
            user.role = data.role;
            if (data.role.name != "") {
                user.admin2 = data.admin2 != null ? data.admin2 : "";
                user.organization = data.organization != null ? data.organization : "";
            }
            context.commit("setOne", user);
            return user;
        });
};

export const update = async(context, data) => {
    return await axios
        .patch(
            resource +
            "/accounts" +
            "/" +
            data.id +
            "?&access_token=" +
            context.rootGetters["session/getToken"],
            data
        )
        .then(response => {
            if (context.state.accounts.length != 0) {
                context.commit("update", data);
            }
            return response.data;
        });
};

export const remove = async(context, data) => {
    return await axios
        .delete(
            resource +
            "/accounts" +
            "/" +
            data +
            "?&access_token=" +
            context.rootGetters["session/getToken"],
            data
        )
        .then(response => {
            context.commit("remove", data);
            return response.data;
        });
};

// roles
export const getRoles = async(context, data) => {
    if (context.state.roles.length == 0) {
        return await axios
            .get(
                resource +
                "/Roles" +
                "?&access_token=" +
                context.rootGetters["session/getToken"]
            )
            .then(response => {
                context.commit("setRoles", response.data);
                return response.data;
            });
    } else {
        return context.state.roles;
    }
};

export const getManagerRoles = async(context, data) => {
    if (context.state.roles.length == 0) {
        return await axios
            .get(
                resource +
                "/Roles" +
                "?&access_token=" +
                context.rootGetters["session/getToken"]
            )
            .then(response => {
                var roles = response.data;
                roles = roles.filter(item => {
                    return item.name !== "coordinator";
                });
                context.commit("setRoles", roles);
                return roles;
            });
    } else {
        return context.state.roles;
    }
};