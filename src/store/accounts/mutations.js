export const set = (state, val) => {
  state.accounts = val;
};

export const update = (state, val) => {
  state.accounts.splice(state.accounts.map(o => o.id).indexOf(val.id), 1, val);
};

export const remove = (state, val) => {
  state.accounts = state.accounts.filter(x => {
    return x.id != val;
  });
};

export const setOne = (state, val) => {
  state.accounts.push(val);
};
export const setLocationId = (state, val) => {
  state.locationId = val;
};

export const setOrganizationId = (state, val) => {
  state.organizationId = val;
};
// roles
export const setRoles = (state, val) => {
  state.roles = val;
};
