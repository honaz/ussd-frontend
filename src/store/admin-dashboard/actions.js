import axios from "axios";
import moment from "moment";
const resource = process.env.ENGINE_API;

export const getAccounts = async (context, data) => {
  if (context.state.accounts == 0) {
    let response = await axios
      .get(
        resource +
          "/accounts/count?&access_token=" +
          context.rootGetters["session/getToken"]
      )
      .then(response => {
        context.commit("setAccounts", response.data.count);
        return response.data.count;
      });
    return response;
  } else {
    return context.state.accounts;
  }
};

export const getByLocation = async (context, data) => {
  if (context.state.accounts == 0) {
    let response = await axios
      .get(
        resource +
          "/accounts/count?&access_token=" +
          context.rootGetters["session/getToken"]
      )
      .then(response => {
        console.log(response);
        context.commit("setAccounts", response.data.count);

        return response.data.count;
      });
    return response;
  } else {
    return context.state.accounts;
  }
};

export const getOrganizations = async (context, data) => {
  if (context.state.organizations == 0) {
    let response = await axios
      .get(
        resource +
          "/organizations/count?&access_token=" +
          context.rootGetters["session/getToken"]
      )
      .then(response => {
        context.commit("setOrganizations", response.data.count);
        return response.data.count;
      });
    return response;
  } else {
    return context.state.organizations;
  }
};

export const getUsageLogs = async (context, date) => {
  if (context.state.usage.length == 0) {
    var d = new Date();
    d.setMonth(d.getMonth() - 6);
    return await axios
      .get(
        resource +
          "/logs?filter[where][and][1][createdon][gt]=" +
          d.toISOString() +
          "&filter[order]=createdon ASC&access_token=" +
          context.rootGetters["session/getToken"]
      )
      .then(response => {
        let usage = [];
        for (let i = 0; i < response.data.length; i++) {
          let log = response.data[i];
          let search = usage.filter(
            item =>
              item.createdon === moment(log.createdon).format("DD-MM-YYYY")
          )[0];
          if (typeof search == "undefined") {
            usage.push({
              createdon: moment(log.createdon).format("DD-MM-YYYY"),
              hits: 1
            });
          } else {
            let newlog = { createdon: search.createdon, hits: search.hits + 1 };
            usage.splice(
              usage.map(o => o.createdon).indexOf(newlog.createdon),
              1,
              newlog
            );
          }
        }
        usage.sort();
        context.commit("setUsage", usage);
        return usage;
      });
  } else {
    return context.state.usage;
  }
};
