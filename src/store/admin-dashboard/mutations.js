export const setUsage = (state, val) => {
  state.usage = val;
};

export const setAccounts = (state, val) => {
  state.accounts = val;
};

export const setOrganizations = (state, val) => {
  state.organizations = val;
};

export const setProjects = (state, val) => {
  state.projects = val;
};

export const setSectors = (state, val) => {
  state.sectors = val;
};

export const setLocations = (state, val) => {
  state.locations = val;
};

export const setLocationId = (state, val) => {
  state.locationId = val;
};
