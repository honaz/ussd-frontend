
    export const getMenu = (state) => {
        return state.menu.slice().reverse();
    }

    export const getMenus = (state) => {
        return state.menus
    }
    
    export const getMenusById = (state, getters) => id => {
        return getters.getMenus .filter(item => item.id === id)[0]
    }

    export const getOptions = (state) => {
        return state.options
    }
    
    export const getOptionsById = (state, getters) => id => {
        return getters.getOptions.filter(item => item.id === id)[0]
    }

  

    
    