
export const setMenu = (state, val) => {
    state.menu = val
}

export const setMenus = (state, val) => {
    state.menus = val
}

export const updateMenus = (state, val) => {
    state.menus.splice(state.stores.map(o => o.id).indexOf(val.id), 1, val);
}

export const removeMenus = (state, val) => {
    state.menus = state.menus.filter(x => {
        return x.id != val;
    })
}

export const setOneMenus = (state, val) => {
    state.menus.push(val)
}



export const setOptions = (state, val) => {
    state.options = val
}

export const updateOptions = (state, val) => {
    state.options.splice(state.stores.map(o => o.id).indexOf(val.id), 1, val);
}

export const removeOptions = (state, val) => {
    state.options = state.stores.filter(x => {
        return x.id != val;
    })
}

export const setOneOptions = (state, val) => {
    state.options.push(val)
}

export const setOptionTypes = (state, val) => {
    state.optionTypes.push(val)
}

