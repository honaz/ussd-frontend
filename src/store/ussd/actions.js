import axios from 'axios'
const resource = process.env.ENGINE_API;


export const getMenus = async (context, id) => {
    let organization = context.rootGetters['session/getOrganization'];
    if (id == null) {
        if (context.state.menus.length == 0) {
            return await axios.get(resource + '/menus' + '?filter={"where":{"organizationId":"'+organization.id+'"}}&access_token=' + context.rootGetters['session/getToken']).then(response => {
                //context.commit('setMenus', response.data);
                return response.data;
            });
        }
        else {
            return context.state.menus;
        }
    }
    else if (id != null) {
        let result = context.getters.getById(id);
        if (typeof result == "undefined") {
            return await axios.get(resource + '/menus' + '/' + id + '?filter={"where":{"organizationId":"'+organization.id+'"}}&access_token=' + context.rootGetters['session/getToken']).then(response => {
                return response.data;
            });
        } else {
            return result;
        }
    }
}

export const getMainMenu = async (context, id) => {
    let organization = context.rootGetters['session/getOrganization'];
    if (context.state.menu.length != null) {
        return await axios.get(resource + '/menus'+'?filter={"where":{"is_main_menu":"true"},"where":{"organizationId":"'+organization.id+'"}}&access_token=' + context.rootGetters['session/getToken']).then(response => {
            //context.commit('setMenu', response.data[0]);
            return response.data[0];
        });
    } else {
        return context.state.menu;
    }
}


export const countMenus = async (context, data) => {
    let organization = context.rootGetters['session/getOrganization'];
    let response = await axios.get(resource + '/menus/count?filter={"where":{"organizationId":"'+organization.id+'"}}&access_token=' + context.rootGetters['session/getToken']).then(response => {
        return response.data;
    });
    return response
}


export const createMenus = async (context, data) => {
    return await axios.post(resource + '/menus' + '?&access_token=' + context.rootGetters['session/getToken'], data).then(response => {
        let menu = response.data;
        //context.commit('setOneMenus', menu);
        return menu;
    });
}

export const updateMenus = async (context, data) => {
    return await axios.patch(resource + '/menus' + '/' + data.id + '?&access_token=' + context.rootGetters['session/getToken'], data).then(response => {
        if (context.state.menus.length != 0) {
           // context.commit('updateMenus', data);
        }
        return response.data;
    });
}

export const removeMenus = async (context, data) => {
    return await axios.delete(resource + '/menus' + '/' + data + '?&access_token=' + context.rootGetters['session/getToken'], data).then(response => {
        //context.commit('removeMenus', data);
        return response.data;
    });
}



// options
export const getOptions = async (context, id) => {

    if (id == null) {
        if (context.state.options.length == 0) {
            return await axios.get(resource + '/options' + '?filter[include]=optionType&access_token=' + context.rootGetters['session/getToken']).then(response => {
                //context.commit('setOptions', response.data);
                return response.data;
            });
        }
        else {
            return context.state.options;
        }
    }
    else if (id != null) {
        let result = context.getters.getById(id);
        if (typeof result == "undefined") {
            return await axios.get(resource + '/options' + '/' + id + '?filter[include]=optionType&access_token=' + context.rootGetters['session/getToken']).then(response => {
                return response.data;
            });
        } else {
            return result;
        }
    }
}

export const getMenuOptions = async (context, id) => {
    if (context.state.menu.length != null) {
        return await axios.get(resource + '/options'+'?filter={"include":"optionType","where":{"menuId":"'+ id+'"}}&access_token=' + context.rootGetters['session/getToken']).then(response => {
            //context.commit('setMenu', response.data[0]);
            return response.data;
        });
    } else {
        return context.state.menu;
    }
}


export const countOptions = async (context, data) => {
    let response = await axios.get(resource + '/options/count?&access_token=' + context.rootGetters['session/getToken']).then(response => {
        return response.data;
    });
    return response
}


export const createOptions = async (context, data) => {
    let optionType = data.optionType;
    delete data.optionType;
    return await axios.post(resource + '/options' + '?&access_token=' + context.rootGetters['session/getToken'], data).then(response => {
        let option = response.data
        option.optionType = optionType;
        //context.commit('setOneOptions', option);
        return option;
    });
}

export const updateOptions = async (context, data) => {
    return await axios.patch(resource + '/options' + '/' + data.id + '?&access_token=' + context.rootGetters['session/getToken'], data).then(response => {
        if (context.state.options.length != 0) {
            //context.commit('updateOptions', data);
        }
        return response.data;
    });
}

export const removeOptions = async (context, data) => {
    return await axios.delete(resource + '/options' + '/' + data + '?&access_token=' + context.rootGetters['session/getToken'], data).then(response => {
        //context.commit('removeOptions', data);
        return response.data;
    });
}

// type
export const getOptionTypes = async (context, data) => {
    if (context.state.optionTypes.length == 0) {
        return await axios.get(resource + '/option_types' + '?&access_token=' + context.rootGetters['session/getToken']).then(response => {
            //context.commit('setOptionTypes', response.data);
            return response.data;
        });
    } else {
        return context.state.types;
    }
}
