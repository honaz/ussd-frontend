export function getAdmin2s (state) {
    return state.admin2s
}

export const getAdmin2sById = (state, getters) => id => {
    return getters.getAdmin2s.filter(item => item.id === id)[0]
}


export function getAdmin3s (state) {
    return state.admin3s
}
