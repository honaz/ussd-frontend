import axios from "axios";
const resource = process.env.ENGINE_API;

export const getOneAdmin2s = async (context, id) => {
  return await axios
    .get(
      resource +
        "/admin_2s" +
        "/" +
        id +
        "?&access_token=" +
        context.rootGetters["session/getToken"]
    )
    .then(response => {
      return response.data;
    });
};

export const getCleanAdmin2s = async (context, id) => {
  if (id == null) {
    if (context.state.admin2s.length == 0) {
      let response = await axios
        .get(
          resource +
            "/admin_2s" +
            "?filter[fields][id]=true&filter[fields][type]=true&filter[fields][properties]=true&access_token=" +
            context.rootGetters["session/getToken"]
        )
        .then(response => {
          context.commit("setAdmin2s", response.data);
          // console.log(response.data);
          return response.data;
        });

      return response;
    } else {
      return context.state.admin2s;
    }
  } else if (id != null) {
    let result = context.getters.getAdmin2sById(id);
    if (typeof result == "undefined") {
      return await axios
        .get(
          resource +
            "/admin_2s" +
            "/" +
            id +
            "?filter[fields][id]=true&filter[fields][type]=true&filter[fields][properties]=true&access_token=" +
            context.rootGetters["session/getToken"]
        )
        .then(response => {
          return response.data;
        });
    } else {
      return result;
    }
  }
};

export const getCleanAdmin3sByAdmin2Id = async (context, id) => {
  let response = await axios
    .get(
      resource +
        "/admin_3s" +
        "?filter[where][properties.ADM2_PCODE]=" +
        id +
        "&filter[fields][id]=true&filter[fields][type]=true&filter[fields][properties]=true&access_token=" +
        context.rootGetters["session/getToken"]
    )
    .then(response => {
      return response.data;
    });
  return response;
};

export const getCleanAdmin3s = async (context, id) => {
  if (id == null) {
    let response = await axios
      .get(
        resource +
          "/admin_3s" +
          "?filter[fields][id]=true&filter[fields][type]=true&filter[fields][properties]=true&access_token=" +
          context.rootGetters["session/getToken"]
      )
      .then(response => {
        return response.data;
      });
    return response;
  } else if (id != null) {
    return await axios
      .get(
        resource +
          "/admin_3s" +
          "/" +
          id +
          "?filter[fields][id]=true&filter[fields][type]=true&filter[fields][properties]=true&access_token=" +
          context.rootGetters["session/getToken"]
      )
      .then(response => {
        return response.data;
      });
  }
};
