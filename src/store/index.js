import Vue from 'vue'
import Vuex from 'vuex'

import session from './session/index'
import accounts from './accounts/index'
import boundaries from './boundaries/index'
import organizations from './organizations/index'
import currency from './currency/index'
import adminDashboard from './admin-dashboard/index'
import ussd from './ussd/index'
import dataStores from './data-stores/index'

Vue.use(Vuex)
Vue.use(require('vue-moment'));
Vue.prototype.$appConfig = {
  name: process.env.APP_NAME,
  name_short: process.env.APP_NAME_SHORT,
  version: process.env.APP_VERSION,
  color: process.env.APP_COLOR
};

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function ({ store }) {
  const Store = new Vuex.Store({
    modules: {
      session,
      accounts,
      boundaries,
      organizations,
      dataStores,
      ussd,
      currency,
      adminDashboard,
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV,
    
  })

  return Store
}
