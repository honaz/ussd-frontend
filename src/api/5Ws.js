import axios from "axios";
const resource = process.env.ENGINE_API;
const apiHost = process.env.ENGINE_API;

export class FiveWs {
  static Admin2Data = [];
  static getProjects() {
    return axios.get(`${apiHost}/projects`).then(item => {
      return item.data;
    });
  }
  //get districts
  static async getAdmin2sById(id) {
    return (await FiveWs.getAdmin2s()).filter(item => item.id == id);
  }

  static getAdmin2s() {
    let admin2Data = [];
    return (function get() {
      if (admin2Data.length == 0) {
        return axios
          .get(
            resource +
              "/admin_2s" +
              '?filter={"fields":{"id": true, "properties":true} }'
          )
          .then(item => {
            admin2Data = item.data;
            return admin2Data;
          });
      }
      return admin2Data;
    })();
  }

  static getValues() {
    return axios.get(resource + "/values").then(item => {
      return item.data;
    });
  }

  static getFields() {
    return axios.get(resource + "/fields").then(item => {
      return item.data;
    });
  }

  static getRecords() {
    return axios.get(resource + "/records").then(item => {
      return item.data;
    });
  }

  //get fieldss
  static getField(id) {
    return axios
      .get(
        resource +
          "/fields" +
          '?filter={"where":{"id":"' +
          id.toString() +
          '"}}'
      )
      .then(item => {
        return item.data;
      });
  }
  //get records
  static getDataRecords(id) {
    return axios
      .get(
        resource +
          "/records" +
          '?filter={"include":["values","account"],"where":{"storeId":"' +
          id.toString() +
          '"}}'
      )
      .then(item => {
        return item.data;
      });
  }
  //get Stores
  static getStores(id) {
    let organization = id;
    return axios
      .get(
        resource +
          "/stores" +
          '?filter={"include":"fields","where":{"id":"' +
          id.toString() +
          '"}}'
      )
      .then(item => {
        return item.data;
      });
  }
  //get storeIds
  static getAllStoresId() {
    return axios
      .get(resource + "/stores" + '?filter={"include":"fields"}')
      .then(item => {
        return item.data;
      });
  }

  static getDashboardData() {
    return FiveWs.getAllStoresId().then(async response => {
      let data = [];

      let organizations = [];
      response.forEach(item => {
        organizations.push({
          organizationId: item.id
        });
      });

      organizations.forEach((item, index) => {
        FiveWs.getDataRecords(item.organizationId).then(response => {
          response.forEach(item => {
            //fetch district name
            FiveWs.getAdmin2sById(item.admin2Id).then(response => {
              var district = "";
              var field = "";
              district = response[0].properties.ADM2_EN;
              //console.log('here', response)

              //fetch value and  field name
              item.values.forEach(item => {
                var field_id = item.fieldId;
                //console.log(field_id);
                FiveWs.getField(field_id).then(async response => {
                  // item.fieldId = item.value
                  field = response[0].name;
                  data.push({
                    district: district,
                    field: field,
                    value: parseFloat(item.value) || 0
                  });

                  //console.log(await data)
                });
                //close getField function
              });
              //close for getAdmin2sById function
            });
            //close forgetDataRecords function
          });
        });
      });

      return data;
    });
  }

  static getLocationsByProjectId(id) {
    return axios
      .get(
        `${apiHost}/projects/${id}/locations?filter={"fields": { "properties": "true"} }`
      )
      .then(item => {
        return item.data;
      });
  }
}
